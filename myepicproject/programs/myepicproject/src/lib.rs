use anchor_lang::prelude::*;

declare_id!("9AfiYT1rPbaeaMZC3dKRYT8jsnAoZdYCr8Zioz543GM1");

#[program]
pub mod myepicproject {
    use super::*;
    pub fn start_stuff_off(ctx: Context<StartStuffOff>) -> ProgramResult {
        let base_account = &mut ctx.accounts.base_account;
        base_account.total_gifs = 0;
        Ok(())
    }

    pub fn add_gif(ctx: Context<AddGif>, gif_link: String) -> ProgramResult {
        let base_account = &mut ctx.accounts.base_account;
        let user = &mut ctx.accounts.user;

        let item = ItemStruct {
            gif_link: gif_link.to_string(),
            user_address: *user.to_account_info().key,
            upvotes_count: 0,
            upvotes_users: Vec::new(),
        };

        base_account.gif_list.push(item);



        base_account.total_gifs += 1;

        Ok(())
    }

    pub fn upvote_gif(ctx: Context<UpvoteGif>, gif_link: String) -> ProgramResult {
        let base_account = &mut ctx.accounts.base_account;
        let user = &mut ctx.accounts.user;

        let search_string = gif_link;
        let found_record = base_account.gif_list.iter_mut().find( |s| s.gif_link == search_string);

        let item = found_record.unwrap();
        
        if item.upvotes_users.contains(&user.to_account_info().key.to_string()) {
            Ok(())
        } else{
            item.upvotes_users.push(user.to_account_info().key.to_string());
            item.upvotes_count += 1;
            Ok(())
        }    

    }
}

#[derive(Accounts)]
pub struct StartStuffOff<'info> {
    #[account(init, payer = user, space = 9000)]
    pub base_account: Account<'info, BaseAccount>,
    #[account(mut)]
    pub user: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
pub struct AddGif<'info> {
    #[account(mut)]
    pub base_account: Account<'info, BaseAccount>,
    #[account(mut)]
    pub user: Signer<'info>,
}

#[derive(Accounts)]
pub struct UpvoteGif<'info> {
    #[account(mut)]
    pub base_account: Account<'info, BaseAccount>,
    #[account(mut)]
    pub user: Signer<'info>,
}

#[derive(Debug, Clone, AnchorSerialize, AnchorDeserialize)]
pub struct ItemStruct {
    pub gif_link: String,
    pub user_address: Pubkey,
    pub upvotes_count: u64,
    pub upvotes_users: Vec<String>,
}

#[account]
pub struct BaseAccount {
    pub total_gifs: u64,
    pub gif_list: Vec<ItemStruct>
}